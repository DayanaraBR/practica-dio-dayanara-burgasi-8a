class Endpoints {
  Endpoints._();

  // url base
  static const String baseUrl = "https://reqres.in/api";

  // tiempo de espera para recibir 
  static const int receiveTimeout = 15000;

  // tiempo de espera para conectarse
  static const int connectionTimeout = 15000;

  static const String users = '/users';
}
